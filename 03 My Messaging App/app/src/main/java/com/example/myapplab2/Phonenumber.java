package com.example.myapplab2;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Phonenumber extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phonenumber);

        // Get the message from the intent
        Intent intent = this.getIntent();
        String message = intent.getStringExtra("EXTRA_MESSAGE");
        // Create the text view
        TextView textView = (TextView) findViewById(R.id.text_message);
        textView.setText("Message:" + message);
    }
    private static final int SEND_MESSAGE_REQUEST_CODE = 0;
    /** Called when the user clicks the Send button */
    public void sendMessage(View view) {
        TextView textView = (TextView) findViewById(R.id.text_message);
        String message = textView.getText().toString();
        EditText editText = (EditText) findViewById(R.id.edit_phone);
        String number = editText.getText().toString();
        Uri uri = Uri.parse("smsto:" + number);
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.putExtra("sms_body", message);
        startActivityForResult(intent, SEND_MESSAGE_REQUEST_CODE);
    }
    /** Called when the user clicks the Send button */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SEND_MESSAGE_REQUEST_CODE){
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, "Message sent!", Toast.LENGTH_LONG).show();
            } else{
                Toast.makeText(this, "Failed to send message or " +
                        "the user has cancel it!", Toast.LENGTH_LONG).show();
}
        }
    }
}