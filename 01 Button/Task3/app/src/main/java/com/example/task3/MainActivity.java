package com.example.task3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import static android.R.id.button1;
import static android.R.id.button2;

public class MainActivity extends AppCompatActivity {

    View.OnClickListener myClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v.getId()==R.id.button1) {
                Toast.makeText(MainActivity.this, "You clicked ONE",
                        Toast.LENGTH_SHORT).show();
            } else if(v.getId()==R.id.button2) {
                Toast.makeText(MainActivity.this, "You clicked TWO",
                        Toast.LENGTH_SHORT).show();
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bt1 = (Button) findViewById(R.id.button1);
        Button bt2 = (Button) findViewById(R.id.button2);
        bt1.setOnClickListener(myClickListener);
        bt2.setOnClickListener(myClickListener);

    }
}
